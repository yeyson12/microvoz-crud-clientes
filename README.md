# microvoz-crud-clientes

#Instrucciones para el uso del proyecto

git clone https://gitlab.com/yeyson12/microvoz-crud-clientes.git

cd microvoz-crud-clientes

composer install

cp .env.example .env

php artisan key:generate

php artisan migrate

php artisan db:seed

php artisan serve
