<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [ 'name', 'lastname', 'email' ,'organization' ,'numberphone' ,'description_customertype' ,'customertype_id' ];

    public function customertype(){
        return $this->belongsTo('App\Models\Customertype','customertype_id','id');
    }
}
