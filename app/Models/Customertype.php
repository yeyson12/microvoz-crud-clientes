<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customertype extends Model
{
    use HasFactory;

	protected $table = 'customertype';

    protected $fillable = [ 'name' ];

    public function customer()
    {
        return $this->hasMany('App\Models\Customer');
    }

}
