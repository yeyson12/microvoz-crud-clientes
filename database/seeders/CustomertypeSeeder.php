<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CustomertypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customertype')->insert([
            'name' => 'Natural'
        ]);

        DB::table('customertype')->insert([
            'name' => 'Empresarial'
        ]);

        DB::table('customertype')->insert([
            'name' => 'Otro'
        ]);
    }
}
