const Welcome = () => import('./components/Welcome.vue' /* webpackChunkName: "resource/js/components/welcome" */)
const CategoryList = () => import('./components/customer/List.vue' /* webpackChunkName: "resource/js/components/category/list" */)
const CategoryCreate = () => import('./components/customer/Add.vue' /* webpackChunkName: "resource/js/components/category/add" */)
const CategoryEdit = () => import('./components/customer/Edit.vue' /* webpackChunkName: "resource/js/components/category/edit" */)

export const routes = [
    {
        name: 'home',
        path: '/',
        component: Welcome
    },
    {
        name: 'categoryList',
        path: '/customer',
        component: CategoryList
    },
    {
        name: 'categoryEdit',
        path: '/customer/:id/edit',
        component: CategoryEdit
    },
    {
        name: 'customerAdd',
        path: '/customer/add',
        component: CategoryCreate
    }
]